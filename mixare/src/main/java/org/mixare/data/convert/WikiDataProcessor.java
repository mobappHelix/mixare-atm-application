/*
 * Copyright (C) 2012- Peer internet solutions & Finalist IT Group
 * 
 * This file is part of mixare.
 * 
 * This program is free software: you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 * 
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <http://www.gnu.org/licenses/>
 */
package org.mixare.data.convert;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mixare.MixContext;
import org.mixare.MixView;
import org.mixare.POIMarker;
import org.mixare.data.DataHandler;
import org.mixare.data.DataSource;
import org.mixare.lib.HtmlUnescape;
import org.mixare.lib.marker.Marker;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.mixare.DataView.mixContext;

/**
 * A data processor for wikipedia urls or data, Responsible for converting raw data (to json and then) to marker data.
 * @author A. Egal
 */
public class WikiDataProcessor extends DataHandler implements DataProcessor{

	public static final int MAX_JSON_OBJECTS = 1000;
	List<Marker> markers = new ArrayList<Marker>();
//	ArrayList<String> array_lat=new ArrayList<>();
//	ArrayList<String> array_lon=new ArrayList<>();
//	ArrayList<String> arr_title=new ArrayList<>();

	ArrayList<String> array_lat;
	ArrayList<String> array_lon;
	ArrayList<String> arr_title;
	//MixContext mixContext;

	public MixContext getContext() {
		return mixContext;
	}

	@Override
	public String[] getUrlMatch() {
		String[] str = {"wiki"};
		return str;
	}

	@Override
	public String[] getDataMatch() {
		String[] str = {"wiki"};
		return str;
	}
	
	@Override
	public boolean matchesRequiredType(String type) {
		if(type.equals(DataSource.TYPE.WIKIPEDIA.name())){
			return true;
		}
		return false;
	}

	@Override
	public List<Marker> load(String rawData, int taskId, int colour) throws JSONException {
		Log.d("load","log in wikiDataProcessor");
		Log.d("taskId", "taskId"+taskId);
		Log.d("colour","colour"+colour);
		markers = new ArrayList<Marker>();
		new dataAsync().execute(""+taskId,""+colour);


		return markers;
	}
	
	private JSONObject convertToJSON(String rawData){
		try {
			return new JSONObject(rawData);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

		public class dataAsync extends AsyncTask<String, Void, String>
		{


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
//			pDialog = new ProgressDialog(Users_Otherthan_You_one.this);
//			pDialog.setMessage("Please wait...");
//			pDialog.setCancelable(false);
//			pDialog.show();

		}

		@Override
		protected String doInBackground(String... arg0) {
			String response = null;
			try {


				Log.v(" test "," context is "+mixContext);
				Location curFix = mixContext.getLocationFinder().getCurrentLocation();
//				LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
				Log.d("lat in wikidata","lat"+curFix.getLatitude());
				Log.d("lng in wikidata","long"+curFix.getLongitude());
				URL url = new URL("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+curFix.getLatitude()+","+curFix.getLongitude()+"&radius=2000&types=atm&name=Goa&key=AIzaSyDvS2MVuJs3H8JX___aM7V0IHdPQ-CIbDs");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				// read the response
				InputStream in = new BufferedInputStream(conn.getInputStream());
				response = convertStreamToString(in);
			} catch (MalformedURLException e) {
				Log.e("", "MalformedURLException: " + e.getMessage());
			} catch (ProtocolException e) {
				Log.e("", "ProtocolException: " + e.getMessage());
			} catch (IOException e) {
				Log.e("", "IOException: " + e.getMessage());
			} catch (Exception e) {
				Log.e("", "Exception: " + e.getMessage());
			}
			return response +"%" +arg0[0]+"%"+arg0[1] ;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				array_lat = new ArrayList<>();
				array_lon=new ArrayList<>();
				arr_title=new ArrayList<>();

				Log.d("result",""+result);
				String[] result1=result.split("%");
				JSONObject jsonObj = new JSONObject(result1[0]);
				JSONArray data_json = jsonObj.getJSONArray("results");
				for (int i = 0; i < data_json.length(); i++)
				{
					JSONObject c = data_json.getJSONObject(i);

					String lat = c.getJSONObject("geometry").getJSONObject("location").getString("lat");
array_lat.add(lat);
					String lon = c.getJSONObject("geometry").getJSONObject("location").getString("lng");
array_lon.add(lon);
					String data_id = c.getString("id");

					String data_name = c.getString("name");
arr_title.add(data_name);

//					Marker ma = null;
//
//
//					Log.v(MixView.TAG, "processing Wikipedia JSON object");
//
//					//no unique ID is provided by the web service according to http://www.geonames.org/export/wikipedia-webservice.html
//					ma = new POIMarker(
//							"",
//							HtmlUnescape.unescapeHTML(data_name, 0),
//							Double.parseDouble(lat),
//							Double.parseDouble(lon),
//							Double.parseDouble("0"),
//							"",
//							Integer.parseInt(result1[1]), Integer.parseInt(result1[2]));
//					markers.add(ma);


				}



				new dataElevationAsync().execute(result1[1],result1[2],array_lat,array_lon,arr_title);



//				Log.d("test", "tag" + array_tag);
//				pDialog.dismiss();

			} catch (Exception e) {
				Log.v(" test ", " catch masala");

				e.printStackTrace();
			}



		}

	}





	public class dataElevationAsync extends AsyncTask<Object, Void, String> {
		ArrayList<String> array_lat1,array_lon1,array_title1;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
//			pDialog = new ProgressDialog(Users_Otherthan_You_one.this);
//			pDialog.setMessage("Please wait...");
//			pDialog.setCancelable(false);
//			pDialog.show();

		}

		@Override
		protected String doInBackground(Object... arg0) {
			String response = null;
			try {
				array_lat1=(ArrayList<String>)arg0[2];
				array_lon1=(ArrayList<String>)arg0[3];
				array_title1=(ArrayList<String>)arg0[4];
				Log.d("array_lat",""+array_lat1.size());
				String abc_lat="";
				for(int i=0;i<array_lat1.size();i++){
					if(i==0){
						abc_lat=array_lat1.get(i)+","+array_lon1.get(i);
					}else{
						abc_lat=abc_lat+"|"+array_lat1.get(i)+","+array_lon1.get(i);
					}

				}

				URL url = new URL("https://maps.googleapis.com/maps/api/elevation/json?locations="+abc_lat+"");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				// read the response
				InputStream in = new BufferedInputStream(conn.getInputStream());
				response = convertStreamToString(in);
			} catch (MalformedURLException e) {
				Log.e("", "MalformedURLException: " + e.getMessage());
			} catch (ProtocolException e) {
				Log.e("", "ProtocolException: " + e.getMessage());
			} catch (IOException e) {
				Log.e("", "IOException: " + e.getMessage());
			} catch (Exception e) {
				Log.e("", "Exception: " + e.getMessage());
			}
			return response +"%" +arg0[0]+"%"+arg0[1] ;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {


				Log.d("result 2    hfnhft  ",""+result);
				String[] result1=result.split("%");

				Log.v(" test "," elevation result is "+result1[0]);
				JSONObject jsonObj = new JSONObject(result1[0]);
				JSONArray data_json = jsonObj.getJSONArray("results");
				Log.d("data_json 2    hfnhft  ",""+data_json.length());
				for (int i = 0; i < data_json.length(); i++) {
					JSONObject c = data_json.getJSONObject(i);

					String elevation = c.getString("elevation");
					Marker ma = null;
					Log.v(MixView.TAG, "processing Wikipedia JSON object");

					//no unique ID is provided by the web service according to http://www.geonames.org/export/wikipedia-webservice.html
					ma = new POIMarker(
							"",
							HtmlUnescape.unescapeHTML(array_title1.get(i), 0),
							Double.parseDouble(array_lat1.get(i)),
							Double.parseDouble(array_lon1.get(i)),
							Double.parseDouble(elevation),
							"",
							Integer.parseInt(result1[1]), Integer.parseInt(result1[2]));
					markers.add(ma);


				}





//				Log.d("test", "tag" + array_tag);
//				pDialog.dismiss();


				Log.v(" test "," lat list "+array_lat1.toString());
				Log.v(" test "," lon list "+array_lon1.toString());
				Log.v(" test "," title is "+array_title1.toString());

			} catch (Exception e) {
				Log.v(" test ", " catch masala");

				e.printStackTrace();
			}



		}

	}


	private String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line).append('\n');
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
}
