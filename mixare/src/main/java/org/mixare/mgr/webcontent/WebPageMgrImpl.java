/*
 * Copyright (C) 2012- Peer internet solutions 
 * 
 * This file is part of mixare.
 * 
 * This program is free software: you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 * 
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <http://www.gnu.org/licenses/>
 */
package org.mixare.mgr.webcontent;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.mixare.MixContext;
import org.mixare.R;

import java.text.DecimalFormat;
import java.util.List;


class WebPageMgrImpl implements WebContentManager {

	protected MixContext mixContext;
	
	/**
	 * Shows a webpage with the given url when clicked on a marker.
	 */
	public void loadMixViewWebPage(String url,Double distance) throws Exception {
		loadWebPage(url,distance ,mixContext.getActualMixView());
	}

	public WebPageMgrImpl(MixContext mixContext) {
       this.mixContext=mixContext;
	}
	
	/* (non-Javadoc)
	 * @see org.mixare.mgr.webcontent.WebContentManager#loadWebPage(java.lang.String, android.content.Context)
	 */
	public void loadWebPage(String url,Double distance, Context context) throws Exception {
		WebView webview = new WebView(context);
		webview.getSettings().setJavaScriptEnabled(true);
Log.d("load web view","m here");
		final Dialog d = new Dialog(context) {
			public boolean onKeyDown(int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK)
					this.dismiss();
				return true;
			}
		};

		webview.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				if (url.endsWith("return")) {
					d.dismiss();
					mixContext.getActualMixView().repaint();
				} else {
					super.onPageFinished(view, url);
				}
			}

		});

		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.getWindow().setGravity(Gravity.BOTTOM);
		d.addContentView(webview, new FrameLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
				Gravity.BOTTOM));

		if (!processUrl(url,distance, mixContext.getActualMixView())) { // if the url could not be processed by
										 // another intent
			d.show();
			webview.loadUrl(url);
		}
	}

	/* (non-Javadoc)
	 * @see org.mixare.mgr.webcontent.WebContentManager#processUrl(java.lang.String, android.content.Context)
	 */
	public boolean processUrl(final String url,Double distance, final Context ctx) {
		// get available packages from the given url
//		List<ResolveInfo> resolveInfos = getAvailablePackagesForUrl(url, ctx);
//		// filter the webbrowser > because the webview will replace it, using
//		// google as simple url
//		List<ResolveInfo> webBrowsers = getAvailablePackagesForUrl(
//				"http://www.google.com", ctx);
//		for (ResolveInfo resolveInfo : resolveInfos) {
//			for (ResolveInfo webBrowser : webBrowsers) { // check if the found
//				Log.d("WebPageMgr","m here");											// intent is not a
//															// webbrowser
//				if (!resolveInfo.activityInfo.packageName
//						.equals(webBrowser.activityInfo.packageName)) {
//					Intent intent = new Intent(Intent.ACTION_VIEW);
//					intent.setData(Uri.parse(url));
//					intent.setClassName(resolveInfo.activityInfo.packageName,
//							resolveInfo.activityInfo.name);
//					ctx.startActivity(intent);
//					return true;
//				}
//			}
//		}
//		return false;
Log.d("url",""+url);
//		DataView.pDialog.dismiss();

final String abc_url[] =url.split("§¶µ");

		final Dialog dialog = new Dialog(ctx);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_data);
//		dialog.setTitle("Title...");

		String textStr="";

		double d = distance;
		DecimalFormat df = new DecimalFormat("@#");
		if(d<1000.0) {
			textStr = " ("+ df.format(d) + "m)";
		}
		else {
			d=d/1000.0;
			textStr =" (" + df.format(d) + "km)";
		}
		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.text);
		TextView text1=(TextView) dialog.findViewById(R.id.text1);
		text.setText(abc_url[1]);
		text1.setText(textStr);
//		text.setText("Android custom dialog example!");
//		ImageView image = (ImageView) dialog.findViewById(R.id.image);
//		image.setImageResource(R.drawable.ic_launcher);

		ImageView dialogButton = (ImageView) dialog.findViewById(R.id.dialogButtonOK);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent navigation = new Intent(Intent.ACTION_VIEW, Uri
						.parse(abc_url[0]));
				ctx.startActivity(navigation);
				dialog.dismiss();
			}
		});


		dialog.show();



//		Uri.Builder builder = new Uri.Builder();
//		builder.scheme("https")
//				.authority("www.google.com").appendPath("maps").appendPath("dir").appendPath("").appendQueryParameter("api", "1")
//				.appendQueryParameter("destination", 15.5228719 + "," + 73.8311006);
//		String url1 = builder.build().toString();
//		Log.d("Directions", url1);
//		Intent i = new Intent(Intent.ACTION_VIEW);
//		i.setData(Uri.parse(url1));
//		ctx.startActivity(i);
//		Uri gmmIntentUri = Uri.parse("geo:15.5377,73.8316");
//		Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//		mapIntent.setPackage("com.google.android.apps.maps");
//		if (mapIntent.resolveActivity(ctx.getPackageManager()) != null) {
//			ctx.startActivity(mapIntent);
//		}
		return true;
	}

	private List<ResolveInfo> getAvailablePackagesForUrl(String url, Context ctx) {
		PackageManager packageManager = ctx.getPackageManager();
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(url));
		return packageManager.queryIntentActivities(intent,
				PackageManager.GET_RESOLVED_FILTER);
	}

}
