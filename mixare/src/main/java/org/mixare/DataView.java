/*
 * Copyright (C) 2010- Peer internet solutions
 * 
 * This file is part of mixare.
 * 
 * This program is free software: you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details. 
 * 
 * You should have received a copy of the GNU General Public License along with 
 * this program. If not, see <http://www.gnu.org/licenses/>
 */
package org.mixare;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mixare.data.DataHandler;
import org.mixare.data.DataSource;
import org.mixare.gui.RadarPoints;
import org.mixare.lib.HtmlUnescape;
import org.mixare.lib.MixUtils;
import org.mixare.lib.gui.PaintScreen;
import org.mixare.lib.gui.ScreenLine;
import org.mixare.lib.marker.Marker;
import org.mixare.lib.render.Camera;
import org.mixare.mgr.downloader.DownloadManager;
import org.mixare.mgr.downloader.DownloadRequest;
import org.mixare.mgr.downloader.DownloadResult;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import static android.view.KeyEvent.KEYCODE_CAMERA;
import static android.view.KeyEvent.KEYCODE_DPAD_CENTER;
import static android.view.KeyEvent.KEYCODE_DPAD_DOWN;
import static android.view.KeyEvent.KEYCODE_DPAD_LEFT;
import static android.view.KeyEvent.KEYCODE_DPAD_RIGHT;
import static android.view.KeyEvent.KEYCODE_DPAD_UP;

/**
 * This class is able to update the markers and the radar. It also handles some
 * user events
 * 
 * @author daniele
 * 
 */
public class DataView {

	private static final String TAG = DataView.class.getSimpleName();
	/** current context */
	public static MixContext mixContext;
	/** is the view Inited? */
	private boolean isInit;

	/** width and height of the view */
	private int width, height;
	public static ProgressDialog pDialog;

	/**
	 * _NOT_ the android camera, the class that takes care of the transformation
	 */
	private Camera cam;

	private MixState state = new MixState();

	/** The view can be "frozen" for debug purposes */
	private boolean frozen;

	/** how many times to re-attempt download */
	private int retry;

	private Location curFix;
	private DataHandler dataHandler = new DataHandler();
	private float radius = 20;

	/** timer to refresh the browser */
	private Timer refresh = null;
	private final long refreshDelay = 45 * 1000; // refresh every 45 seconds

	private boolean isLauncherStarted;

	private ArrayList<UIEvent> uiEvents = new ArrayList<UIEvent>();

	private RadarPoints radarPoints = new RadarPoints();
	private ScreenLine lrl = new ScreenLine();
	private ScreenLine rrl = new ScreenLine();
	private float rx = 10, ry = 20;
	private float addX = 0, addY = 0;
	
	private List<Marker> markers;


	ArrayList<String> array_lat;
	ArrayList<String> array_lon;
	ArrayList<String> arr_title;

	/**
	 * Constructor
	 */
	public DataView(MixContext ctx) {
		this.mixContext = ctx;
	}

	public MixContext getContext() {
		return mixContext;
	}

	public boolean isLauncherStarted() {
		return isLauncherStarted;
	}

	public boolean isFrozen() {
		return frozen;
	}

	public void setFrozen(boolean frozen) {
		this.frozen = frozen;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public DataHandler getDataHandler() {
		return dataHandler;
	}

	public boolean isDetailsView() {
		return state.isDetailsView();
	}

	public void setDetailsView(boolean detailsView) {
		state.setDetailsView(detailsView);
	}

	public void doStart() {
		state.nextLStatus = MixState.NOT_STARTED;
		mixContext.getLocationFinder().setLocationAtLastDownload(curFix);
	}

	public boolean isInited() {
		return isInit;
	}

	public void init(int widthInit, int heightInit) {
		try {
			width = widthInit;
			height = heightInit;

			cam = new Camera(width, height, true);
			cam.setViewAngle(Camera.DEFAULT_VIEW_ANGLE);

			lrl.set(0, -RadarPoints.RADIUS);
			lrl.rotate(Camera.DEFAULT_VIEW_ANGLE / 2);
			lrl.add(rx + RadarPoints.RADIUS, ry + RadarPoints.RADIUS);
			rrl.set(0, -RadarPoints.RADIUS);
			rrl.rotate(-Camera.DEFAULT_VIEW_ANGLE / 2);
			rrl.add(rx + RadarPoints.RADIUS, ry + RadarPoints.RADIUS);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		frozen = false;
		isInit = true;
	}

	public void requestData(String url) {
		DownloadRequest request = new DownloadRequest(new DataSource(
				"LAUNCHER", url, DataSource.TYPE.MIXARE,
				DataSource.DISPLAY.CIRCLE_MARKER, true));
		mixContext.getDataSourceManager().setAllDataSourcesforLauncher(
				request.getSource());
		mixContext.getDownloadManager().submitJob(request);
		state.nextLStatus = MixState.PROCESSING;
		}


//	public void requestData(DataSource datasource, double lat, double lon, double alt, float radius, String locale) {
//		DownloadRequest request = new DownloadRequest();
//		request.params = datasource.createRequestParams(lat, lon, alt, radius, locale);
//		request.source = datasource;
//		
//		mixContext.getDownloadManager().submitJob(request);
//		state.nextLStatus = MixState.PROCESSING;
//	}

	public void draw(PaintScreen dw) {
		mixContext.getRM(cam.transform);
		curFix = mixContext.getLocationFinder().getCurrentLocation();

		state.calcPitchBearing(cam.transform);

		// Load Layer
		if (state.nextLStatus == MixState.NOT_STARTED && !frozen) {
			loadDrawLayer();
			markers = new ArrayList<Marker>();
		}
		else if (state.nextLStatus == MixState.PROCESSING) {
//			DownloadManager dm = mixContext.getDownloadManager();
//			DownloadResult dRes = null;
//
//			markers.addAll(downloadDrawResults(dm, dRes));

				new dataAsync().execute("" + (-1), "" + (-65536));

//			if (dm.isDone()) {

//			}
		}

		// Update markers
		dataHandler.updateActivationStatus(mixContext);
		for (int i = dataHandler.getMarkerCount() - 1; i >= 0; i--) {
			Marker ma = dataHandler.getMarker(i);
			// if (ma.isActive() && (ma.getDistance() / 1000f < radius || ma
			// instanceof NavigationMarker || ma instanceof SocialMarker)) {
			if (ma.isActive() && (ma.getDistance() / 1000f < radius)) {

				// To increase performance don't recalculate position vector
				// for every marker on every draw call, instead do this only
				// after onLocationChanged and after downloading new marker
				// if (!frozen)
				// ma.update(curFix);
				Log.d("frozen",""+frozen);
				Log.d("cam",""+cam);
				Log.d("addx",""+addX);
				Log.d("addy",""+addY);
				Log.d("dw",""+dw);
				if (!frozen) {
					ma.calcPaint(cam, addX, addY);
//					AlertDialog.Builder builder;
//					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//						builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
//					} else {
//						builder = new AlertDialog.Builder(getContext());
//					}
//					builder.setTitle("Delete entry")
//							.setMessage("Are you sure you want to delete this entry?")
//							.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog, int which) {
//									// continue with delete
//								}
//							})
//							.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog, int which) {
//									// do nothing
//								}
//							})
//							.setIcon(android.R.drawable.ic_dialog_alert)
//							.show();
				}
				ma.draw(dw);
			}
		}

		// Draw Radar
		drawRadar(dw);

		// Get next event
		UIEvent evt = null;
		synchronized (uiEvents) {
			if (uiEvents.size() > 0) {
				evt = uiEvents.get(0);
				uiEvents.remove(0);
			}
		}
		if (evt != null) {
			switch (evt.type) {
			case UIEvent.KEY:
				handleKeyEvent((KeyEvent) evt);
				break;
			case UIEvent.CLICK:
				Log.d("click","event");
				handleClickEvent((ClickEvent) evt);
				break;
			}
		}
		state.nextLStatus = MixState.PROCESSING;
	}

	/**
	 * Part of draw function, loads the layer.
	 */
	private void loadDrawLayer(){
		Log.d("stattus url",""+mixContext.getStartUrl().length());
		Log.d("stattus url",""+mixContext.getStartUrl());
		if (mixContext.getStartUrl().length() > 0) {
			requestData(mixContext.getStartUrl());
			isLauncherStarted = true;
		}

		else {
			double lat = curFix.getLatitude(), lon = curFix.getLongitude(), alt = curFix
					.getAltitude();
			state.nextLStatus = MixState.PROCESSING;
			mixContext.getDataSourceManager().requestDataFromAllActiveDataSource(lat, lon, alt,	radius);
		}

		// if no datasources are activated
		if (state.nextLStatus == MixState.NOT_STARTED)
			state.nextLStatus = MixState.DONE;
	}
	
	private List<Marker> downloadDrawResults(DownloadManager dm, DownloadResult dRes){
		List<Marker> markers = new ArrayList<Marker>();
		while ((dRes = dm.getNextResult()) != null) {
			if (dRes.isError() && retry < 3) {
				retry++;
//				mixContext.getDownloadManager().submitJob(
//						dRes.getErrorRequest());
				// Notification
				// Toast.makeText(mixContext, dRes.errorMsg,
				// Toast.LENGTH_SHORT).show();
			}

			if(!dRes.isError()) {
				if(dRes.getMarkers() != null){
					//jLayer = (DataHandler) dRes.obj;
					Log.i(MixView.TAG,"Adding Markers");
					markers.addAll(dRes.getMarkers());
Log.d("dRes type",""+dRes.getDataSource().getType());
					// Notification
					Toast.makeText(
							mixContext,
							mixContext.getResources().getString(
									R.string.download_received)
									+ " " + dRes.getDataSource().getName(),
							Toast.LENGTH_SHORT).show();
				}
			}
		}
		return markers;
	}
	

	/**
	 * Handles drawing radar and direction.
	 * Paint screen screen that radar will be drawn to
	 */
	private void drawRadar(PaintScreen dw) {
		String dirTxt = "";
		int bearing = (int) state.getCurBearing();
		int range = (int) (state.getCurBearing() / (360f / 16f));
		// TODO: get strings from the values xml file
		if (range == 15 || range == 0)
			dirTxt = getContext().getString(R.string.N);
		else if (range == 1 || range == 2)
			dirTxt = getContext().getString(R.string.NE);
		else if (range == 3 || range == 4)
			dirTxt = getContext().getString(R.string.E);
		else if (range == 5 || range == 6)
			dirTxt = getContext().getString(R.string.SE);
		else if (range == 7 || range == 8)
			dirTxt = getContext().getString(R.string.S);
		else if (range == 9 || range == 10)
			dirTxt = getContext().getString(R.string.SW);
		else if (range == 11 || range == 12)
			dirTxt = getContext().getString(R.string.W);
		else if (range == 13 || range == 14)
			dirTxt = getContext().getString(R.string.NW);

		radarPoints.view = this;
		dw.paintObj(radarPoints, rx, ry, -state.getCurBearing(), 1);
		dw.setFill(false);
		dw.setColor(Color.argb(150, 0, 0, 220));
		dw.paintLine(lrl.x, lrl.y, rx + RadarPoints.RADIUS, ry
				+ RadarPoints.RADIUS);
		dw.paintLine(rrl.x, rrl.y, rx + RadarPoints.RADIUS, ry
				+ RadarPoints.RADIUS);
		dw.setColor(Color.rgb(255, 255, 255));
		dw.setFontSize(12);

		radarText(dw, MixUtils.formatDist(radius * 1000), rx
				+ RadarPoints.RADIUS, ry + RadarPoints.RADIUS * 2 - 10, false);
		radarText(dw, "" + bearing + ((char) 176) + " " + dirTxt, rx
				+ RadarPoints.RADIUS, ry - 5, true);
	}

	private void handleKeyEvent(KeyEvent evt) {
		/** Adjust marker position with keypad */
		final float CONST = 10f;
		switch (evt.keyCode) {
		case KEYCODE_DPAD_LEFT:
			addX -= CONST;
			break;
		case KEYCODE_DPAD_RIGHT:
			addX += CONST;
			break;
		case KEYCODE_DPAD_DOWN:
			addY += CONST;
			break;
		case KEYCODE_DPAD_UP:
			addY -= CONST;
			break;
		case KEYCODE_DPAD_CENTER:
			frozen = !frozen;
			break;
		case KEYCODE_CAMERA:
			frozen = !frozen;
			break; // freeze the overlay with the camera button
		default: //if key is set, then ignore event
				break;
		}
	}

	boolean handleClickEvent(ClickEvent evt) {
		final int interval = 1000; // 1 Second
//		Handler handler = new Handler();
//		Runnable runnable = new Runnable(){
//			public void run() {
//				Toast.makeText(mixContext, "", Toast.LENGTH_SHORT).show();
//			}
//		};
//
//		handler.postAtTime(runnable, System.currentTimeMillis()+interval);
//		handler.postDelayed(runnable, interval);
//		pDialog = new ProgressDialog(DataView.mixContext);
//		pDialog.setMessage("Please wait...");
//		pDialog.setCancelable(false);
//		pDialog.show();
		try {
			Thread.sleep(5000);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean evtHandled = false;
Log.d("Dataview log","for click"+state.nextLStatus);
		Log.d("Dataview 1log","for click"+MixState.DONE);
		Log.d("Dataview 2log","for click"+MixState.NOT_STARTED);
		Log.d("Dataview 3log","for click"+MixState.PROCESSING);
		Log.d("Dataview 4log","for click"+MixState.READY);

		// Handle event
		if (state.nextLStatus == MixState.DONE) {
			// the following will traverse the markers in ascending order (by
			// distance) the first marker that
			// matches triggers the event.
			//TODO handle collection of markers. (what if user wants the one at the back)
			for (int i = 0; i < dataHandler.getMarkerCount() && !evtHandled; i++) {
				Marker pm = dataHandler.getMarker(i);



						evtHandled = pm.fClick(evt.x, evt.y, mixContext, state);

			}
		}
		return evtHandled;
	}

	private void radarText(PaintScreen dw, String txt, float x, float y, boolean bg) {
		float padw = 4, padh = 2;
		float w = dw.getTextWidth(txt) + padw * 2;
		float h = dw.getTextAsc() + dw.getTextDesc() + padh * 2;
		Log.d("m","here draw");
		if (bg) {
			dw.setColor(Color.rgb(0, 0, 0));
			dw.setFill(true);
			dw.paintRect(x - w / 2, y - h / 2, w, h);
			dw.setColor(Color.rgb(255, 255, 255));
			dw.setFill(false);
			dw.paintRect(x - w / 2, y - h / 2, w, h);
		}
		dw.paintText(padw + x - w / 2, padh + dw.getTextAsc() + y - h / 2, txt,
				false);
	}

	public void clickEvent(float x, float y) {
		synchronized (uiEvents) {
			uiEvents.add(new ClickEvent(x, y));
		}
	}

	public void keyEvent(int keyCode) {
		synchronized (uiEvents) {
			uiEvents.add(new KeyEvent(keyCode));
		}
	}

	public void clearEvents() {
		synchronized (uiEvents) {
			uiEvents.clear();
		}
	}

	public void cancelRefreshTimer() {
		if (refresh != null) {
			refresh.cancel();
		}
	}
	
	/**
	 * Re-downloads the markers, and draw them on the map.
	 */
	public void refresh(){
		state.nextLStatus = MixState.NOT_STARTED;
	}
	
	private void callRefreshToast(){
		mixContext.getActualMixView().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(
						mixContext,
						mixContext.getResources()
								.getString(R.string.refreshing),
						Toast.LENGTH_SHORT).show();
			}
		});
	}

	public class dataAsync extends AsyncTask<String, Void, String>
	{


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
//			pDialog = new ProgressDialog(Users_Otherthan_You_one.this);
//			pDialog.setMessage("Please wait...");
//			pDialog.setCancelable(false);
//			pDialog.show();

		}

		@Override
		protected String doInBackground(String... arg0) {
			String response = null;
			try {

				Log.d("retry","here6: ");
				Log.v(" test "," context is "+mixContext);
				Location curFix = mixContext.getLocationFinder().getCurrentLocation();
//				LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
				Log.d("lat in wikidata","lat"+curFix.getLatitude());
				Log.d("lng in wikidata","long"+curFix.getLongitude());
				URL url = new URL("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+curFix.getLatitude()+","+curFix.getLongitude()+"&radius=2000&types=atm&name=Goa&key=AIzaSyDvS2MVuJs3H8JX___aM7V0IHdPQ-CIbDs");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				// read the response
				InputStream in = new BufferedInputStream(conn.getInputStream());
				response = convertStreamToString(in);
				Log.d("retry","here7: ");
				response="{\n" +
						"   \"html_attributions\" : [],\n" +
						"   \"next_page_token\" : \"CqQCGQEAAK-GbfFgcD3xYfRWy4odVWjbzlhn9bNF_l_NOzAQLSFrofqrA88X1yzPl3rrRF0tHeH9Vl237a3O8kMRCYzsn3MQe-mCUfT2H8if82KejoAcaEsFLQJSqHN6IKWAgYI6YJ122CEZHh0Y6dH0yxC6L6RNyrjOsw4udc3NRQcy9GiIDLpNnPhT7tBOKgialMjW5ZXFvhDsrF_Z-bIsSi7L0HqtZ7cvaW5wJ2BvtMucjxOUcjmTuL4bxRjvrtPSXfKVNrZDP84zVDVlO3u7js1KxLscHb7wHqa1-VNcRR745vEdcJrpQO-lsA8eE7WfRQJQ5uYCdrqQ21_CfhdWXqkz-WgeFX497fh0iKaXZPviTwe7maXAEIe-fy0M-kRk6v_G6BIQ7uHCfGvc_1v88GIHNp17JhoUVPZhfY6P-vy-9J-XZk6EaL1sDY4\",\n" +
						"   \"results\" : [\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.4976653,\n" +
						"               \"lng\" : 73.82709939999999\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.4989791802915,\n" +
						"                  \"lng\" : 73.82846918029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4962812197085,\n" +
						"                  \"lng\" : 73.82577121970849\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"fcb73dc1cbf35ee48f112ddce06a1b1c2277406c\",\n" +
						"         \"name\" : \"Punjab National Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 538,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/107778520077375025974/photos\\\"\\u003eKylie D&#39;Souza\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAAVj69EGwW08sCDaUmip-mFiYVfkYYS1M9NgmBiauAaXTdlTQh0Ayv0X2KKO2QteaFRsr2JOKRA0ADU3154BNW1I7v-DHmbIYoEDc1MM_R9oJfWrSicDP2HsY34Ml1qnivEhAtioMWUNBRSVeMNocyasIuGhQCvAc0E523lnXGgtVGNnhwpTkcWg\",\n" +
						"               \"width\" : 720\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJzZhC3o7AvzsRNcmiIQxDIko\",\n" +
						"         \"reference\" : \"CmRRAAAApAQMlrxcgTRZEv9kjCs4DZaUNIwFmmhute8i4eoGFq12T_gU0XiKGqc2whOEF5AsxfwFr0RSovxpHy_gUHO6KPvsauJeRaZl0snMyv_Cip4Aji37m888uonerjt6fj7JEhA2eQD1FaB052_hml7uQUwQGhTEmwbtx79oeYSq7ICOEXPBANR3wQ\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Doctor Atmaram Borkar Road, Altinho, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.4973579,\n" +
						"               \"lng\" : 73.8233559\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.49870688029151,\n" +
						"                  \"lng\" : 73.82470488029149\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4960089197085,\n" +
						"                  \"lng\" : 73.82200691970849\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"900a4a5a8e32773dd2232917622185f51b634c78\",\n" +
						"         \"name\" : \"Vijaya Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 2592,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/101104515250179783865/photos\\\"\\u003eSuhas Sadekar\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAAi9QMB7g7U53rB_ffCWPhN42u7MVgk9HsrOfvfV2iWJdPc1Q4cMJ1CXgk7erdedBUkBmo85rfwW2nP6oVuWKIPVHPFufKiZYcMFNUJXcJTD5EUZZGJIrUN2mYDWM8HpRcEhCS23L0AvcOPXRMSAXM1imEGhRe66IT5f7OmwoWN0DqX5zFnCJ_jA\",\n" +
						"               \"width\" : 4608\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJlVEfPYzAvzsRECW9GoHjkik\",\n" +
						"         \"reference\" : \"CmRRAAAA1mULvONZG34lLsej8gE20PKiCzj0Pf6RGa8tfkDFNTBkvtnArk13X8F08azDHFmMfr3eLPI6bIirL-M9Q5XZyiqn51pf2po75yWMDAsZ0ITb_uwo50UopalxUaHBQMUfEhD4-WC1dHQSArEvoS9LANg9GhTfPXvPDLJEP-2od74_mt_hREp4QA\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"bank\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"JDDM Hall Building, H S Road, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.496483,\n" +
						"               \"lng\" : 73.82373699999999\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.4978319802915,\n" +
						"                  \"lng\" : 73.8250859802915\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4951340197085,\n" +
						"                  \"lng\" : 73.82238801970848\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"835c2a303d860ed6d5770a37b19940de16270a59\",\n" +
						"         \"name\" : \"HDFC Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 2592,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/101104515250179783865/photos\\\"\\u003eSuhas Sadekar\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAALAxA9q0_dgmdo0K4r0u3v3bjbcdGIYw0Ss61jhmFp09klYSdHPDVaVjE_roVN8LekGWpm72joGWqeLhYrF0IN7oLpoKrTOkT5bOxxgRC0D_azaNhfaUYLI_cWqsWIdG5EhD1rNfuGlqn_Bu1CTRtI2exGhT_IYrBW5lqOIVXnv-O79On36Ujcg\",\n" +
						"               \"width\" : 4608\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJV5QYyo3AvzsRF0H15w7R1II\",\n" +
						"         \"rating\" : 5,\n" +
						"         \"reference\" : \"CmRSAAAAlQNWXhQ08TSAuTZfPvFmiHt6QpaFVKPV14t0ASVq6rCwHbuHs9J_tIbxBiUXhi-iDqhs9el2s5xTLxTOMmBIJvVobxJyWctd61K7SVYRvoBgwNaKJpDSbn0B2zqSpntXEhAobdHT_XidE-OTrCWn55sgGhSqkwqA7-hPFKvfBIiUvZT8puQViA\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"bank\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"SN 1, Manjunath Bldg, 18 June Rd, Panjim, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.579815,\n" +
						"               \"lng\" : 73.7987529\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.5812042802915,\n" +
						"                  \"lng\" : 73.80006903029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.5785063197085,\n" +
						"                  \"lng\" : 73.7973710697085\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"26aa195e50b9ee3143a9c553325322f1af9c6505\",\n" +
						"         \"name\" : \"Indian Overseas Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 1434,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/111588619843691862786/photos\\\"\\u003eSanjay Rawat\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAACOIpnevhSDoXnNgzMpTRTfPhvaGNOnNLspJ3m6RbA8bt2qwnmmVrsWTzI8usw7x2Vu0Gv_nlhWg0RUmShWvsyLjMSowLiMV4guAXg1EMFZp-ncjJqHGQmFlPuLQso_TeEhC50WlZUZ5AX0Wdl2CMqMQOGhSwqCbkVC5NlujbpP9DmNjJ4Fcsaw\",\n" +
						"               \"width\" : 2549\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJNeatG_fqvzsRobRh9yAjMBU\",\n" +
						"         \"rating\" : 4.5,\n" +
						"         \"reference\" : \"CmRRAAAAnJvDK4KcGx7TD25pFxtaivxhaa8nH3gsUcUJSNFbd9TaxL_1O10CsKav6uUjIxJWiUkku2YDXITUohS1nXLKF_JMqmaNjfwnZzCsUqtBMT6-91SEQYkmurdY73gW6PqpEhD0Lw8CLPfAO3eLx9qn7hevGhTVc1shD-7qXWmjoJQ1KdR_b8SMQg\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Calangute - Mapusa Road, Parra, Canca, Mapusa, Goa\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.5281964,\n" +
						"               \"lng\" : 73.8283823\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.5295453802915,\n" +
						"                  \"lng\" : 73.82973128029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.5268474197085,\n" +
						"                  \"lng\" : 73.82703331970851\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"5f4b49f751a07d935c579a207a90898d7ee8fda8\",\n" +
						"         \"name\" : \"HDFC Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 563,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/100868154194318409931/photos\\\"\\u003eHDFC Bank ATM\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAAjIoDtvkFbRmr9ofGyXw5QE_b6MUprd8V95KqERBF-8IoGxBd7SBfCHwQQG69EG13hR01smY0jw2qjrfvmjpC1aQ8Hu4i1iBfXrqO_07xYxNNd9vAt44JlMdK8XA3_T4wEhB5HOMFjvPfJgXJt6FbGlaKGhSf4X8p4yt8N-Fk8eLwHgJ9ZFSNTA\",\n" +
						"               \"width\" : 1000\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJhVwnyhLAvzsRXp-bDm2XL_w\",\n" +
						"         \"reference\" : \"CmRSAAAAojyCirI8w2vSwNNVyiqviIOnFRlVPfgOVRx8JWwubfj5xPCw_HzZG5mttZ_OEOKUR-EyFPQIWYatZcJJCEcW47BE3Ev0gp8WVuhwlV4GxVLfTd1MOo7-fhVCfCv0P5xpEhA09FDl49cL2A3tfESWZUhwGhSW5XNmOXa-Z72DV8-D-V63VUD1MQ\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"bank\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"HN 406/148, PN 138, Porvorim, Bardez, Goa\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.5308569,\n" +
						"               \"lng\" : 73.82811269999999\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.5321553302915,\n" +
						"                  \"lng\" : 73.82945318029149\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.52945736970849,\n" +
						"                  \"lng\" : 73.82675521970849\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"197312b79cdd13df2072cb542624894e40a76fdf\",\n" +
						"         \"name\" : \"Vijaya Bank ATM - Porvorim Branch\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"place_id\" : \"ChIJgz_RWhLAvzsRPf3zsofr3U0\",\n" +
						"         \"rating\" : 3,\n" +
						"         \"reference\" : \"CmRRAAAA8iOlRLbnxK3kjECvL61nHI26Fd2908kKanjo3VGhv8-TmZgI_i4NaV4ELsNKaPA1Tcfmuq5kmuDVeXeY4YrJUF3UuiyMXwNTN18jLJZskc9cfxpcMyFBI1SwqSdSFK5wEhALPeYOUNdXf4nzmkXn4y_sGhQdqh0SjP-SNbYdm25dl8dJlgxg2A\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"bank\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Shop No. 03,04 & 05, Yougman Residency, BB Borkar Road, Alto Porvorim, Penha de França\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.499021,\n" +
						"               \"lng\" : 73.82628099999999\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.5003699802915,\n" +
						"                  \"lng\" : 73.82762998029149\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4976720197085,\n" +
						"                  \"lng\" : 73.82493201970848\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"b2b73890cfbc365ec88c98b72d84c6d5c16f0a9a\",\n" +
						"         \"name\" : \"Indian Overseas Bank Atm Panaji Branch\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"place_id\" : \"ChIJpwgoM4nAvzsR3KUIHjI_1yw\",\n" +
						"         \"reference\" : \"CmRRAAAAEOSQlUOTQiStK2e5yUZtg3-jZrSH4lDiQhJUEfXHDuvU2pht9BVNKkhCWK-A5iY00PKi7s--0w9cN6mpwA1jd8MWM1tutasmW-efq2fHc1zcE9kwFmI_Hy2J2d-0dNE3EhAn_8FuUHLrJG6vsb_PaI5TGhRE7CpKwpEoKz4UxH6p3HJQTr2m4Q\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Neptune Hotel Buildings, Dr. A. Pissurlkar Road, Ozari, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.497353,\n" +
						"               \"lng\" : 73.8359\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.4987019802915,\n" +
						"                  \"lng\" : 73.83724898029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4960040197085,\n" +
						"                  \"lng\" : 73.83455101970848\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"d8751f829f631d23f74b1b0e0b18810a039b5dd8\",\n" +
						"         \"name\" : \"Axis Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"place_id\" : \"ChIJPazxeITAvzsRepmIwZjaBVs\",\n" +
						"         \"reference\" : \"CmRRAAAADpQgLybZzNwmVd8dRd7oFkA29yRTev_EP4Y82ZbKpb9z5Mf7UmcfD9aN8nVk4KJJcxiUuKKWOq6SBadUO6N2JhbHf0Ga9Oj6IUVkpo_ttTuM_p7nMM_VTXaHnCGwU_8kEhBwgQLkcZGNHdSmIgtjW_DyGhTVZytSLWcXZA3fvanrw7DbnV11aQ\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Cardozo Building, Next To Paulo Travels, Near Panaji Bus Stand, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.5271839,\n" +
						"               \"lng\" : 73.8264061\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.5285328802915,\n" +
						"                  \"lng\" : 73.82775508029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.5258349197085,\n" +
						"                  \"lng\" : 73.82505711970849\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"d0f1e541037fc109d8e204e15c5d16a3dc348b18\",\n" +
						"         \"name\" : \"Corporation Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 3120,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/112505794177731126429/photos\\\"\\u003ejoy fernandes\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAA5OJ3lktfAudRLWMUm8mccGvIlD0-v7EEuFiNDtW9fyxNwIBvh7Qsacao3RtBWzbtjqod69sHwj2TlicjKtIpfZ6ae1j63rZMG7LBwwzuIZCrJyv4fSVJoYaL_vKGsddvEhCwsMaLNusQ2sKg3iqVd0KRGhQt8GEsXI_Vqa_Gk84Trj6zwEuUlw\",\n" +
						"               \"width\" : 4160\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJn-5-0mzAvzsRMPGVr5lNyXw\",\n" +
						"         \"rating\" : 1.7,\n" +
						"         \"reference\" : \"CmRRAAAAfJi1QdFtKk1W545BlzDV1UZnFsTC4ik2UR0fLFnwnkN3WaBcXKVLY45_KwSE4m-wezL49yCyPrPlSOXy_7W1yttTmoFpTgz_3iRjioapkUtAihOZHJ2OJyXlWHzmMB_bEhDG4IBzjP7z_G00lGw_wAzuGhRo31OyeUBa35EKvxV8Y0SeCDMWmw\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"National Highway 17, Alto Porvorim, Penha de França\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.4975,\n" +
						"               \"lng\" : 73.83579999999999\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.4988489802915,\n" +
						"                  \"lng\" : 73.8371489802915\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4961510197085,\n" +
						"                  \"lng\" : 73.8344510197085\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png\",\n" +
						"         \"id\" : \"88755349b065037b606db5d55aae8bd529826f24\",\n" +
						"         \"name\" : \"ICICI Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"place_id\" : \"ChIJeVGPVoTAvzsRSPwrE1flF4I\",\n" +
						"         \"rating\" : 3,\n" +
						"         \"reference\" : \"CmRSAAAArxIcALnfS6MB3u-DuGHGS7azfTroF3Nu1FCOqkcK2Ry8qpx7E3yiEf31486jZ7a3hDFvU1c3WCu8EZVQqlI0yfzeqF91AL7c-9DUUXDUKksvKnwrWX087LscMiYUSM2pEhC4KDbXjEjRbNim4Tz7Zv0kGhRBXzXxoy7yu0kB8hFUQsgeUALcnA\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Opp Uti Atm, Near Bus Stand, Panjim, Pato, Panjim, Cesa Goa, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.4981459,\n" +
						"               \"lng\" : 73.825436\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.4992515302915,\n" +
						"                  \"lng\" : 73.8269348802915\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4965535697085,\n" +
						"                  \"lng\" : 73.82423691970848\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"bc61013ee65ae25c101e21b147977cabc101a207\",\n" +
						"         \"name\" : \"Oriental Bank of Commerce ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 4032,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/114564481886438552862/photos\\\"\\u003eRufus John\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAAi4DNXKAgWANb0Pf0yRF8d6EReswG0rWapsF9KiRfGvkVtCfkHZYp5WcuE0bicKf3O06Ex5hdk_ufnhKPefiCnvz2Na3d7SIlzFIbOU4evAvuSEh9AqH4dJGLtUQwgnogEhABJZ-lQGrVLXBEQ6JndxtOGhTHFuUx2fA3EBx4qPgPYD9LrXcxlA\",\n" +
						"               \"width\" : 3024\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJtTHpuY7AvzsRsWi75WxaxSo\",\n" +
						"         \"reference\" : \"CmRRAAAAiZkzicOVLG6-zOBcbhL8i4vld3NIzxkfoW9wJkD7vSl9Ei5m7syMs11yrBYsDlkdyWhyiaMNO__XnknaVtnVMdxXQwpA7i-LTIDLg-rmiSeDyfsDhy_3J-fz9vR7KVnfEhD0FyQvju_3v4S37p7iemdoGhRoLsrV1kXRaERHdNKag2eyaJuAiQ\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Sapane Regency, 18th June Road, Near Titan Showroom, Ozari, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.4941179,\n" +
						"               \"lng\" : 73.82073500000001\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.4954427302915,\n" +
						"                  \"lng\" : 73.82215658029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4927447697085,\n" +
						"                  \"lng\" : 73.8194586197085\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"f81e9bffb8e9b59dfbd5527e25859f5b21a3d270\",\n" +
						"         \"name\" : \"Karnataka Bank Ltd. ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"place_id\" : \"ChIJ4bJXco3AvzsRSuaTX8xpnoQ\",\n" +
						"         \"rating\" : 5,\n" +
						"         \"reference\" : \"CmRSAAAAKsxf-KUxtn14yuPN8lgexfGHdbiJtPBb5HCwnPJSo1i5e5kS_oLmk0H17pAyLXIxKAmEW3bbNbRhlEc9tWxcW5iXyAhemCwpC1zdweeMayeJZajysoFEo8RS8Yl8HU3MEhDOKIg7b2HHXhBBLMb1kzkQGhSyxCeccNZjPgF0JKLYILfel6W8NA\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Karim Building, Mahatma Gandhi Road, Near Hotel Vivanta, Altinho, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.534656,\n" +
						"               \"lng\" : 73.8241719\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.5360049802915,\n" +
						"                  \"lng\" : 73.82552088029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.5333070197085,\n" +
						"                  \"lng\" : 73.8228229197085\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"8cc2a4be283f5e0f17c74ac4a1b880180bcc74bc\",\n" +
						"         \"name\" : \"Punjab National Bank ATM - Succorro Branch\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 2304,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/101104515250179783865/photos\\\"\\u003eSuhas Sadekar\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAAChVK8CzVIcXezJXzBaAHXnv1-PnJhe1rg0lOyE7V3cOs5YBAgEiwAMlL_c8qwVqHvVeKx3_OxmU6_yJZjFwFIppvn7lkmR7p95lqaDlR0V38sZCZlmOUvHaSSC-3iaTzEhDg5HbJz3bTOXPfVwHZRuTtGhTIqIUc-hMPSnT4BDYO1nT79iyY9A\",\n" +
						"               \"width\" : 4096\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJTfNdyxbAvzsRN4Epevpp3Og\",\n" +
						"         \"rating\" : 4,\n" +
						"         \"reference\" : \"CmRSAAAAvt_XkRuKPpHcishzhiREvqNkRL7LkWV14Ah9eQQP6TcUiYV8ZpEclH6j9NZYXJvBTEJfIODYj3gqHiIVznvweY3Ud3ChGFWscrTWA3WMuyR_YRuES0pnVB3wSbtM1ZE2EhCmwFLUYjQ-rIIMg-2pcHLVGhQTv67EucMqQT-Fz4biMAHhBZdMQg\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"NH - 17, Cosme Cesta Nucleus, Opp. Holy Family Church, Succorro, Alto Povorim, Penha de França\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.496505,\n" +
						"               \"lng\" : 73.82628699999999\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.4978819802915,\n" +
						"                  \"lng\" : 73.82769138029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4951840197085,\n" +
						"                  \"lng\" : 73.82499341970851\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"d50da621b37aa39d6677ec4a4611100ff2114bb3\",\n" +
						"         \"name\" : \"Axis Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"place_id\" : \"ChIJw-3y6o7AvzsReycrzahku-g\",\n" +
						"         \"rating\" : 3,\n" +
						"         \"reference\" : \"CmRSAAAAe-yEwOCnbMmaiFXKr5hSSANCgd4hicT7ItdksiQvXYBGHBEtQr9N70X13GvxHNAAwSgsNuLyqMH1bVDCq0lUGdO2MVmsFpKSvTwakd-_0Hzb69DpNbOEJA65OZeSXys8EhAO3Do6q-iiXlxbVxbFDxoaGhRR__C6ECO0E2qALMJ2nScSZkovrw\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"SIDHARTH BANDODKAR BHAVANP SHIRGAONKAR ROADPANJIM GOA403101, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.5228719,\n" +
						"               \"lng\" : 73.8311006\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.5242208802915,\n" +
						"                  \"lng\" : 73.83244958029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.5215229197085,\n" +
						"                  \"lng\" : 73.82975161970849\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"d3c8a619f7a7f495cb00079672e8ea587b0154b3\",\n" +
						"         \"name\" : \"Central Bank Of India ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"place_id\" : \"ChIJgXOUEHLAvzsR8GccNTOCem4\",\n" +
						"         \"rating\" : 1,\n" +
						"         \"reference\" : \"CmRRAAAAeUsxDwo4iYwa8LNZDocCKRJlkLKFVV4B-cQ_rWojeScq2iEuSl9ufKasV1xcS2pM8XIi9xe5ClIyG2GCQ3ppYy91MWG66poC6_CjBBlYMnkV2HbBYu2fY-zYZhGWsKgOEhCSiwA9MetkDfRxsuQ-KmyQGhSzXLAPNZrNjqqhra860vGI_rHWLg\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Alto Porvorim, Penha de França\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.500969,\n" +
						"               \"lng\" : 73.8270309\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.50241263029151,\n" +
						"                  \"lng\" : 73.8283740802915\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4997146697085,\n" +
						"                  \"lng\" : 73.82567611970849\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"7d4b1e67a10fdd2999b5b5a6ffbc3e002450fda7\",\n" +
						"         \"name\" : \"State Bank of India ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 1152,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/109971053784309571843/photos\\\"\\u003eLaximan Vengurlekar\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAApJzAQ22x3kZK4op3QYEgqdV3eGdT89QcsH9CqRyshEVnt4P5sxwYWTrox8YdP2_fc6rFkt0MZ9uapEK7BapQo7YaQU7hNuQClk1I1I_l6lwcWIUFlTcmxl47DOCa4y6EEhB06ul9O4KOh01J_XJVzm3NGhR6isnhTnAUgw0wPbSjmzA6z2saDQ\",\n" +
						"               \"width\" : 2048\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJB0O3HYnAvzsRbPGvxQ8zTc4\",\n" +
						"         \"reference\" : \"CmRSAAAAzYWU6BdqhJiQ3Ht4hEtCvqQW7ZfQMjxMMGCWn1PSu_hsF9kG8R-HsC6wYUU3L6I4L7f-QGbzqrK-YcxFIZLKXQby-WQ3eA24rZPDehuU167FPc269OPhJl-fp-roBwzWEhD3V76dzcrv8fSotRky5SLZGhTVMGclxNpsq0wBUilc7BuGMOjiaA\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Dayanand Bandodkar Marg, Opp. Hotel Mandovi, Altinho, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.5243324,\n" +
						"               \"lng\" : 73.8268976\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.5256813802915,\n" +
						"                  \"lng\" : 73.82824658029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.52298341970849,\n" +
						"                  \"lng\" : 73.82554861970848\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"f0c20e1dd5acd75c7c0f96476c42cfa0d4b18c0c\",\n" +
						"         \"name\" : \"ICICI Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 1407,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/101104515250179783865/photos\\\"\\u003eSuhas Sadekar\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAAoObMibn_2YFiIFSqzIgPxxvB2nHza3aYhKF3u0SWObmuDTs-WGaZWTurcdhIM5CUL6HjLWiGg_w1fhnpowx8tSgPXWkBLXy9TkTOUyQpweTvciyWcoM0vSKJw9q4KQucEhAJPuvLkZVdjKK4msoZgySCGhS70mC70nYrV3SX5c4TMjZ2v-Splg\",\n" +
						"               \"width\" : 2022\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJvQDcORPAvzsRnOUSCODA9IM\",\n" +
						"         \"rating\" : 3.5,\n" +
						"         \"reference\" : \"CmRSAAAAC5o07fjuHRoe1-jMATNl5qL3aZ0p0-iCYBBOO-4MxDmuNw7pgnZ_bZdJQTiEcVgapApKeAT5Q0t2NtrxSUgKGVYCqCPf7kGrVPCCCFsl4k0yxYgv-IR0tmYlfXt9VJ0QEhCCV32wLO5mHs4Q5GWzHYTkGhSeLQPio8ig_0s9LKLdNr_X3lGZsg\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"bank\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Near P&T Quarters Kalika, Alto Porvorim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.4932914,\n" +
						"               \"lng\" : 73.8184801\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.4946403802915,\n" +
						"                  \"lng\" : 73.81982908029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4919424197085,\n" +
						"                  \"lng\" : 73.81713111970851\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"5335f1be9d6f6d2f34b5191c22aaf10fab131970\",\n" +
						"         \"name\" : \"IDBI ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"place_id\" : \"ChIJIUJLx4vAvzsRLRXWuYsWCS4\",\n" +
						"         \"reference\" : \"CmRRAAAAjJYkICFkM-aRk_6vVI-vw3HpIWw-tu96y0jd6FzNO1hFla4Kfs8p8ArcigdC5ocZIEJe8CFXzVBuzkmVurA8XL1wiwf-66G9C8Ll8amYoSE412Mo_6Na-aANf4qjwbz3EhARQsYHtsRV9Jrp-CTx6iXdGhQAnwSJWlJv_YAHsWGiE_KlxU1seg\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Alcon Chambers, Dayanand Bandodkar Marg, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.496496,\n" +
						"               \"lng\" : 73.8253622\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.4978449802915,\n" +
						"                  \"lng\" : 73.82671118029151\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.4951470197085,\n" +
						"                  \"lng\" : 73.82401321970849\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"7f3054de9be65371af3a81cf5527ce154716c494\",\n" +
						"         \"name\" : \"ICICI BANK ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 2340,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/107778530688490571938/photos\\\"\\u003eAkhil Rane\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAAuAACfhBRnIznBuMd-UotHtfeTWT_7ZaTZWeyCFAXiMgCLEmz3nl1YomrXmoOizyICgjDPVSPX0zU5g695ulQ7W39wlfTj8EIzCLMiyB2fTi9rVjSSjO-0gtpTs15i2cgEhBHbQgNZ6KuG3O0MIzt55WoGhSRWoXjbkcvOtNKnPSASyzjStxBkA\",\n" +
						"               \"width\" : 4160\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJfV5hD43AvzsRBTeZltlpisI\",\n" +
						"         \"rating\" : 5,\n" +
						"         \"reference\" : \"CmRSAAAA5wlCXhT0sAFZgpzlZKPbL88G6-9ylTI9TDJgw8wssJyyJ9LdMiP9zo2DEENrSGS7TSOjslDk38YofjzcoNAg5FI6YQMW4BRw2FlCg5jxqQSC90gA7oHKat45MwvitsFCEhAsSpcYpidcR03gpd5PEOf2GhTAD_yOvA9i6kCNivaPwCqfAQFxbg\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"bank\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"Panaji(ebor) Branch, Daulat Building, Panjim\"\n" +
						"      },\n" +
						"      {\n" +
						"         \"geometry\" : {\n" +
						"            \"location\" : {\n" +
						"               \"lat\" : 15.50275269999999,\n" +
						"               \"lng\" : 73.81054569999999\n" +
						"            },\n" +
						"            \"viewport\" : {\n" +
						"               \"northeast\" : {\n" +
						"                  \"lat\" : 15.5041016802915,\n" +
						"                  \"lng\" : 73.8118946802915\n" +
						"               },\n" +
						"               \"southwest\" : {\n" +
						"                  \"lat\" : 15.50140371970849,\n" +
						"                  \"lng\" : 73.80919671970848\n" +
						"               }\n" +
						"            }\n" +
						"         },\n" +
						"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
						"         \"id\" : \"e2e905fb18cc5c9ed335b1cb57d10dd0bb18b1c5\",\n" +
						"         \"name\" : \"HDFC Bank ATM\",\n" +
						"         \"opening_hours\" : {\n" +
						"            \"open_now\" : true,\n" +
						"            \"weekday_text\" : []\n" +
						"         },\n" +
						"         \"photos\" : [\n" +
						"            {\n" +
						"               \"height\" : 563,\n" +
						"               \"html_attributions\" : [\n" +
						"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/118306663961671822580/photos\\\"\\u003eHDFC Bank ATM\\u003c/a\\u003e\"\n" +
						"               ],\n" +
						"               \"photo_reference\" : \"CmRaAAAAx5dVKAPnyCCYb0Luzpgz4QbIVNHVvMk4mOnxTZtNG5NBxCNVsAS6pvJpgL_IFRws0pnZEh80BZeOoC-_BjWB-8-DXylpL_p3PHdrvSZ2dbvWJxfBxv5IRdBkdESTO4y0EhAehmOW1zK3jvCcYNEYHjcdGhRxUMHFh0ElvGa-obV3FvU7gL-4Zw\",\n" +
						"               \"width\" : 1000\n" +
						"            }\n" +
						"         ],\n" +
						"         \"place_id\" : \"ChIJ9_TqR_bAvzsR1Wz7DfckLAM\",\n" +
						"         \"reference\" : \"CmRRAAAAU_C1jMODi9LL73KNvyvdykmxC-bDmCZjhFwbTfinnyXQ2k1zKSmV5hYqfYr5oKjrj4UJlYJ7mA55S28yMW7JovX_kEOl7qVXjAoJ_j8aJNe3UuDpRCzwijoc5ee3ztQ0EhCpbPOT-im2I67Q4bt8nQO4GhSNSNNEpwowKB-W1nxCDt_rX86M_g\",\n" +
						"         \"scope\" : \"GOOGLE\",\n" +
						"         \"types\" : [ \"atm\", \"bank\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
						"         \"vicinity\" : \"SN LG 3, Zenith Classic, Reis Magos, Verem, Verem\"\n" +
						"      }\n" +
						"   ],\n" +
						"   \"status\" : \"OK\"\n" +
						"}";
			} catch (MalformedURLException e) {
				Log.e("", "MalformedURLException: " + e.getMessage());
			} catch (ProtocolException e) {
				Log.e("", "ProtocolException: " + e.getMessage());
			} catch (IOException e) {
				Log.e("", "IOException: " + e.getMessage());
			} catch (Exception e) {
				Log.e("", "Exception: " + e.getMessage());
			}
			return response +"%" +arg0[0]+"%"+arg0[1] ;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				Log.d("retry","here8: ");
				array_lat = new ArrayList<>();
				array_lon=new ArrayList<>();
				arr_title=new ArrayList<>();

				Log.d("result",""+result);
				String[] result1=result.split("%");
				JSONObject jsonObj = new JSONObject(result1[0]);

				JSONArray data_json = jsonObj.getJSONArray("results");
				for (int i = 0; i < data_json.length(); i++)
				{
					JSONObject c = data_json.getJSONObject(i);

					String lat = c.getJSONObject("geometry").getJSONObject("location").getString("lat");
					array_lat.add(lat);
					String lon = c.getJSONObject("geometry").getJSONObject("location").getString("lng");
					array_lon.add(lon);
					String data_id = c.getString("id");

					String data_name = c.getString("name");
					arr_title.add(data_name);

//					Marker ma = null;
//
//
//					Log.v(MixView.TAG, "processing Wikipedia JSON object");
//
//					//no unique ID is provided by the web service according to http://www.geonames.org/export/wikipedia-webservice.html
//					ma = new POIMarker(
//							"",
//							HtmlUnescape.unescapeHTML(data_name, 0),
//							Double.parseDouble(lat),
//							Double.parseDouble(lon),
//							Double.parseDouble("0"),
//							"",
//							Integer.parseInt(result1[1]), Integer.parseInt(result1[2]));
//					markers.add(ma);


				}



				new dataElevationAsync().execute(result1[1],result1[2],array_lat,array_lon,arr_title);



//				Log.d("test", "tag" + array_tag);
//				pDialog.dismiss();

			} catch (Exception e) {
				Log.v(" test ", " catch masala");

				e.printStackTrace();
			}



		}

	}





	public class dataElevationAsync extends AsyncTask<Object, Void, String> {
		ArrayList<String> array_lat1,array_lon1,array_title1;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
//			pDialog = new ProgressDialog(Users_Otherthan_You_one.this);
//			pDialog.setMessage("Please wait...");
//			pDialog.setCancelable(false);
//			pDialog.show();

		}

		@Override
		protected String doInBackground(Object... arg0) {
			String response = null;
			try {
				Log.d("retry","here9: ");
				array_lat1=(ArrayList<String>)arg0[2];
				array_lon1=(ArrayList<String>)arg0[3];
				array_title1=(ArrayList<String>)arg0[4];
				Log.d("array_lat",""+array_lat1.size());
				String abc_lat="";
				for(int i=0;i<array_lat1.size();i++){
					if(i==0){
						abc_lat=array_lat1.get(i)+","+array_lon1.get(i);
					}else{
						abc_lat=abc_lat+"|"+array_lat1.get(i)+","+array_lon1.get(i);
					}

				}

				URL url = new URL("https://maps.googleapis.com/maps/api/elevation/json?locations="+abc_lat+"");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				// read the response
				InputStream in = new BufferedInputStream(conn.getInputStream());
				response = convertStreamToString(in);
				Log.d("retry","here10: ");
			} catch (MalformedURLException e) {
				Log.e("", "MalformedURLException: " + e.getMessage());
			} catch (ProtocolException e) {
				Log.e("", "ProtocolException: " + e.getMessage());
			} catch (IOException e) {
				Log.e("", "IOException: " + e.getMessage());
			} catch (Exception e) {
				Log.e("", "Exception: " + e.getMessage());
			}
			return response +"%" +arg0[0]+"%"+arg0[1] ;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				Log.d("retry","here11: ");
				Log.d("result 2    hfnhft  ",""+result);
				String[] result1=result.split("%");

				Log.v(" test "," elevation result is "+result1[0]);
				JSONObject jsonObj = new JSONObject(result1[0]);
				JSONArray data_json = jsonObj.getJSONArray("results");
				Log.d("data_json 2    hfnhft  ",""+data_json.length());
				for (int i = 0; i < data_json.length(); i++) {
					JSONObject c = data_json.getJSONObject(i);

					String elevation = c.getString("elevation");
					Marker ma = null;
					Log.v(MixView.TAG, "processing Wikipedia JSON object");



					//no unique ID is provided by the web service according to http://www.geonames.org/export/wikipedia-webservice.html


					ma = new POIMarker(
							"",
							HtmlUnescape.unescapeHTML(array_title1.get(i), 0),
							Double.parseDouble(array_lat1.get(i)),
							Double.parseDouble(array_lon1.get(i)),
							Double.parseDouble(elevation),
							"http://maps.google.com/maps?daddr="+array_title1.get(i)+"§¶µ" + array_title1.get(i),
							(-1), (-65536));

//					ma = new POIMarker(
//							"",
//							HtmlUnescape.unescapeHTML(array_title1.get(i), 0),
//							Double.parseDouble(array_lat1.get(i)),
//							Double.parseDouble(array_lon1.get(i)),
//							Double.parseDouble(elevation),
//							"http://maps.google.com/maps?saddr=" +
//									+ curFix.getLatitude() + ", " + curFix.getLongitude() + "(Current Location)" + "&daddr=" +
//									array_lat1.get(i) + "," + array_lon1.get(i)+ "("+array_title1.get(i)+")",
//							(-1), (-65536));

//					ma = new POIMarker(
//							"",
//							HtmlUnescape.unescapeHTML(array_title1.get(i), 0),
//							Double.parseDouble(array_lat1.get(i)),
//							Double.parseDouble(array_lon1.get(i)),
//							Double.parseDouble(elevation),
//							"",
//							Integer.parseInt(result1[1]), Integer.parseInt(result1[2]));
					markers.add(ma);

MixView.crash_check=false;
				}

				Log.d("retry","here12: ");
				retry = 0;
				state.nextLStatus = MixState.DONE;

				dataHandler = new DataHandler();
				dataHandler.addMarkers(markers);
				Log.d("",curFix.getLatitude()+ " * "+curFix.getLongitude());
				dataHandler.onLocationChanged(curFix);

//				if (refresh == null) { // start the refresh timer if it is null
//					refresh = new Timer(false);
//					Date date = new Date(System.currentTimeMillis()
//							+ refreshDelay);
//					refresh.schedule(new TimerTask() {
//
//						@Override
//						public void run() {
//							callRefreshToast();
//							refresh();
//						}
//					}, date, refreshDelay);
//				}


//				Log.d("test", "tag" + array_tag);
//				pDialog.dismiss();

				Log.d("retry","here13: ");
				Log.v(" test "," lat list "+array_lat1.toString());
				Log.v(" test "," lon list "+array_lon1.toString());
				Log.v(" test "," title is "+array_title1.toString());

			} catch (Exception e) {
				Log.v(" test ", " catch masala");

				e.printStackTrace();
			}



		}

	}


	private String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line).append('\n');
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}

class UIEvent {
	public static final int CLICK = 0;
	public static final int KEY = 1;

	public int type;
}

class ClickEvent extends UIEvent {
	public float x, y;

	public ClickEvent(float x, float y) {
		this.type = CLICK;
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
}

class KeyEvent extends UIEvent {
	public int keyCode;

	public KeyEvent(int keyCode) {
		this.type = KEY;
		this.keyCode = keyCode;
	}

	@Override
	public String toString() {
		return "(" + keyCode + ")";
	}



}

